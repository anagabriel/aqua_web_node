// Include the cluster module
let cluster = require('cluster');

// Code to run if we're in the master process
if (cluster.isMaster) {

  // Count the machine's CPUs
  let cpuCount = require('os').cpus().length;

  // Create a worker for each CPU
  for (let i = 0; i < cpuCount; i += 1) {
    cluster.fork();
  }

  // Listen for terminating workers
  cluster.on('exit', (worker) => {
    // Replace the terminated workers
    console.log('Worker ' + worker.id + ' died :(');
    cluster.fork();
  });

// Code to run if we're in a worker process
} else {
  let compression = require('compression');
  let express = require('express');
  let path = require('path');
  let favicon = require('serve-favicon');
  let logger = require('morgan');
  let cookieParser = require('cookie-parser');
  let bodyParser = require('body-parser');
  let cors = require('cors');

  let index = require('./routes/index');

  let app = express();

  // view engine setup
  app.set('views', path.join(__dirname, 'views'));
  app.set('view engine', 'hjs');

  // uncomment after placing your favicon in /public
  app.use(compression());
  app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
  app.use(logger('dev'));
  app.use(bodyParser.json());
  app.use(bodyParser.urlencoded({ extended: false }));
  app.use(cookieParser());
  app.use(express.static(path.join(__dirname, 'public')));
  app.use(cors());

  app.use('/', index);

  // catch 404 and forward to error handler
  app.use((req, res, next) => {
    let err = new Error('Not Found');
    err.status = 404;
    next(err);
  });

  // error handler
  app.use((err, req, res) => {
    // set locals, only providing error in development
    res.locals.message = err.message;
    res.locals.error = err;

    // render the error page
    res.status(err.status || 500);
    res.render('error');
  });

  let port = process.env.PORT || 3000;
  let server = app.listen(port, (err) => {
    if (err) {
      console.error(err);
    } else {
      console.log('Server running at http://127.0.0.1:' + port + '/');
    }
  });
}
