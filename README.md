# AQUA - Node.js

## Pre-requisites
* [Git](https://git-scm.com/downloads)
* [Node.js](https://nodejs.org/en/download/)
* [npm](https://www.npmjs.com/get-npm)
* [Grunt](https://gruntjs.com/getting-started)
  * [For ppl who think things like Grunt are weird and hard](https://24ways.org/2013/grunt-is-not-weird-and-hard/)
  * [Generate multi-resolution images for srcset](https://addyosmani.com/blog/generate-multi-resolution-images-for-srcset-with-grunt/)
  * [Grunt responsive images](https://github.com/andismith/grunt-responsive-images)
* [MongoDB](https://docs.mongodb.com/manual/installation/)
  * [Mac](https://gist.github.com/nrollr/9f523ae17ecdbb50311980503409aeb3)
  * [Mongoose](http://mongoosejs.com/docs/guide.html)

Run the following commands to check if they're installed:
```shell
$ which git
# it should give a path like...
/usr/bin/git

$ which node
# it should give a path like...
/usr/local/bin/node

$ which npm
# it should give a path like...
/usr/local/bin/npm

$ which grunt
# it should give a path like...
/usr/local/bin/grunt

$ which mongo
# it should give a path like...
/usr/local/bin/mongo
```

## Install AQUA
Run the following command to install the project.
```shell
$ git clone https://gitlab.com/anagabriel/aqua_web_node.git

# you'll get some output like...
Cloning into 'aqua_web_node'...
remote: Enumerating objects: 6, done.
remote: Counting objects: 100% (6/6), done.
remote: Compressing objects: 100% (4/4), done.
remote: Total 6 (delta 0), reused 0 (delta 0)
Unpacking objects: 100% (6/6), done.

$ npm i
# some output

$ npm start
# some input that tells you where to route to via client
```

## Update AQUA
To make a change to aqua, run these commands
```shell
$ npm run build
# some output

$ grunt
# some output

$ git add -A
# some output

$ git commit -m "Comment that tells what you updated within the repo"
# some output

$ git push
# some output
```
