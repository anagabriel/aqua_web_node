var path = require('path');

module.exports = {
  mode: 'development',
  entry: {
    app: './src/App.js',
    rest: './src/Aunt.js'
  },
  output: {
    path: path.resolve(__dirname, 'public/js'),
    filename: '[name].js',
    sourceMapFilename: '[name].map'
  },
  devtool: '#source-map',
  module: {
   // configuration regarding modules
   rules: [
     // rules for modules (configure loaders, parser options, etc.)
     {
       // flags to apply these rules, even if they are overridden (advanced option)
       loader: 'babel-loader',
       // the loader which should be applied, it'll be resolved relative to the context
       // -loader suffix is no longer optional in webpack2 for clarity reasons
       // see webpack 1 upgrade guide
       options: {
         presets: ['react', 'es2015']
       },
       // options for the loader
     }
   ]
 }
}
