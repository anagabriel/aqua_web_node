let express = require('express');
let router = express.Router();

let multer = require('multer');
let Excel = require('exceljs');
// these files will show up in /public/uploads
let upload = multer({ dest: 'uploads/' });

/* GET home page. */
router.get('/', (req, res, next) => {
  res.render('index', { title: 'Express' });
});

/* POST example post for files */
router.post('/', upload.single('file'), (req, res) => {
  //text fields
  console.log(req.body);

  //file contents
  console.log(req.file);

  let wb = new Excel.Workbook();
  let local_file = './public/uploads/' + req.file.filename;

  wb.xlsx.readFile(local_file).then(() => {
    let sh = wb.getWorksheet("Sheet1");

    sh.getRow(1).getCell(2).value = 32;

    // write the edited sheet to a different file
    wb.xlsx.writeFile("sample2.xlsx");
    console.log("Row-3 | Cell-2 - " + sh.getRow(3).getCell(2).value);

    console.log(sh.rowCount);
    //Get all the rows data [1st and 2nd column]
    for (let i = 1; i <= sh.rowCount; i++) {
      console.log(sh.getRow(i).getCell(1).value);
      console.log(sh.getRow(i).getCell(2).value);
    }
  });

  // process
  let response = 'Something was done.';
  res.json(response);
});

module.exports = router;
